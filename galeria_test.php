<?php

$dir = 'path/para/galeria';
$ficha = scandir($dir);
$ficha = array_diff($ficha, array('..','.'));
$ficha = array_values($ficha);

// --------------- prueba 1 que separa en 2 arrays el array principal de ficheros
//
// $par = array();
// $non = array();
// foreach( $ficha as $key => $value ) {
//     if( 0 === $key%2) {
//         $par[] = $value;
//     }
//     else {
//         $non[] = $value;
//     }
// }

// ---------------- continuacion de prueba 1: guardar arrays par y non en dos nuevos arrays complejos luego de sustituir ciertos caracteres
//
// $obra_par = array();
// $obra_non= array();
// $prx = array("[", "]");
// for ($i = 0; $i < count($par); $i++){
//
//   preg_match("!(.)(.*?)\((.*?)\)(\[.*\])!", $par[$i], $result);
//   $obra_par[$i]['archivo'] = $par[$i];
//   $obra_par[$i]['posicion'] = $result[1];
//   $obra_par[$i]['nombre'] = ucwords(str_replace('_',' ',$result[2]));
//   $obra_par[$i]['tecnica'] = $result[3];
//   $obra_par[$i]['precio'] = str_replace($prx,'',$result[4]);
// }
// for ($i = 0; $i < count($non); $i++){
//
//   preg_match("!(.)(.*?)\((.*?)\)(\[.*\])!", $non[$i], $result);
//   $obra_non[$i]['archivo'] = $non[$i];
//   $obra_non[$i]['posicion'] = $result[1];
//   $obra_non[$i]['nombre'] = ucwords(str_replace('_',' ',$result[2]));
//   $obra_non[$i]['tecnica'] = $result[3];
//   $obra_non[$i]['precio'] = str_replace($prx,'',$result[4]);
// }

// ------------------------------ FOR WHILE IF de 2 en 2: par y non
//
// for ($i = 0; $i < count($ficha); $i++) {
//   while (0 === $i%2) {
//     print $i." ";
//     $count++;
//     if ($count == 2) {
//      print "</br>";
//       $count = 0;
//     }
//     break;
// }
// }
// 
// echo '</br>';
// 
// for ($i = 0; $i < count($ficha); $i++) {
//   while (1 === $i%2) {
//     print $i." ";
//     $count++;
//     if ($count == 2) {
//      print "</br>";
//       $count = 0;
//     }
//     break;
// }
// }

?>